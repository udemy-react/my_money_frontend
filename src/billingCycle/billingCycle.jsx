import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'

import Tabs from '../common/tab/tabs'
import TabsHeader from '../common/tab/tabsHeader'
import TabHeader from '../common/tab/tabHeader'
import TabsContent from '../common/tab/tabsContent'
import TabContent from '../common/tab/tabContent'
import { create, update, remove, init } from './billingCycleActions'

import BillingCycleList from './billingCycleList'
import BillingCycleForm from './billingCycleForm'

class BillingCycle extends Component {

    componentWillMount() {
        this.props.init()
    }

    render() {
        return (
            <div>
                <ContentHeader title="Ciclos de Pagamentos" small="Cadastro" />

                <Content>
                    <Tabs>
                        <TabsHeader>
                            <TabHeader icon='bars' label='Listar' target='tabList' />
                            <TabHeader icon='plus' label='Incluir' target='tabCreate' />
                            <TabHeader icon='pencil' label='Editar' target='tabUpdate' />
                            <TabHeader icon='trash-o' label='Excluir' target='tabDelete' />
                        </TabsHeader>

                        <TabsContent>
                            <TabContent id='tabList'>
                                <BillingCycleList />
                            </TabContent>

                            <TabContent id='tabCreate'>
                                <BillingCycleForm onSubmit={this.props.create} submitClass='primary' submitLabel='Incluir' />
                            </TabContent>

                            <TabContent id='tabUpdate'>
                                <BillingCycleForm onSubmit={this.props.update} submitClass='info' submitLabel='Alterar' />
                            </TabContent>

                            <TabContent id='tabDelete'>
                                <BillingCycleForm onSubmit={this.props.remove} submitClass='danger' submitLabel='Excluir' readOnly='true' />
                            </TabContent>
                        </TabsContent>
                    </Tabs>
                </Content>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ create, update, remove, init }, dispatch)

export default connect(null, mapDispatchToProps)(BillingCycle)