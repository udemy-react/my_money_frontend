import React from 'react'

export default props => (
    <footer className='main-footer'>
        <b>
            Copyright 2018
            &nbsp;
            <a href="http://www.alvoideal.com.br" target="_blank">Alvo Ideal</a>
        </b>
    </footer>
)