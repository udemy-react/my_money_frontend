const initialState = { selected: '', visible: {} }

export default (state = initialState, action) => {
    switch (action.type) {
        case "TAB_SHOWED":
            return { ...state, visible: action.payload }
        case "TAB_SELECTED":
            return { ...state, selected: action.payload }
        default:
            return state
    }
}